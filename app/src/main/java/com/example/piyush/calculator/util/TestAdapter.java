package com.example.piyush.calculator.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piyush.calculator.R;
import com.example.piyush.calculator.main.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by PIYUSH on 22-01-2015.
 */
public class TestAdapter extends BaseAdapter {
    private ArrayList summaryList = new ArrayList();
    Context ctx;

    public TestAdapter(Context ctx, ArrayList<String> data) {
        this.ctx = ctx;
        this.summaryList = data;
    }

    @Override
    public int getCount() {
        return summaryList.size();
    }

    @Override
    public Map.Entry<Character, String> getItem(int position) {
        return (Map.Entry) summaryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item, null);
        TextView exp = (TextView) row.findViewById(R.id.expression);
        exp.setText(summaryList.get(position).toString());
        return row;
    }
}
