package com.example.piyush.calculator.util;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.piyush.calculator.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class SummaryActivity extends ActionBarActivity {
    TestAdapter listAdapter;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        final ListView listView = (ListView) findViewById(R.id.summary_list);

        final Spinner spinner = (Spinner) findViewById(R.id.filter);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.filter, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinner.setSelection(position);
                switch ((String) spinner.getSelectedItem()) {
                    case "All":
                        list = Data.summaryMap.get("all");
                        Collections.reverse(list);
                        listAdapter = new TestAdapter(getApplicationContext(), list);
                        listView.setAdapter(listAdapter);
                        break;
                    case "Add":
                        list = Data.summaryMap.get("add");
                        Collections.reverse(list);
                        listAdapter = new TestAdapter(getApplicationContext(), list);
                        listView.setAdapter(listAdapter);
                        break;
                    case "Subtract":
                        list = Data.summaryMap.get("subtract");
                        Collections.reverse(list);
                        listAdapter = new TestAdapter(getApplicationContext(), list);
                        listView.setAdapter(listAdapter);
                        break;
                    case "Multiply":
                        listAdapter = new TestAdapter(getApplicationContext(), Data.summaryMap.get("multiply"));
                        listView.setAdapter(listAdapter);
                        break;
                    case "Divide":
                        list = Data.summaryMap.get("divide");
                        Collections.reverse(list);
                        listAdapter = new TestAdapter(getApplicationContext(), list);
                        listView.setAdapter(listAdapter);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
