package com.example.piyush.calculator.main;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.piyush.calculator.R;
import com.example.piyush.calculator.util.Data;

public class MainActivity extends ActionBarActivity {

    EditText editNumber1, editNumber2;
    TextView editResult;
    Double number1, number2, result = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Data.summaryMap.put("all", Data.allList);
        Data.summaryMap.put("add", Data.addList);
        Data.summaryMap.put("subtract", Data.subtractList);
        Data.summaryMap.put("multiply", Data.multiplyList);
        Data.summaryMap.put("divide", Data.divideList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClick(View v) {
        if (v.getId() == R.id.summary) {
            summary();

        } else {
            editNumber1 = (EditText) this.findViewById(R.id.number1);
            editNumber2 = (EditText) this.findViewById(R.id.number2);
            editResult = (TextView) this.findViewById(R.id.result);
            String n1 = editNumber1.getText().toString();
            String n2 = editNumber2.getText().toString();
            if (n1.isEmpty() || n2.isEmpty()) {
                Dialog dialog = new Dialog(this);
                dialog.setTitle("Fields cannot be blank");
                dialog.show();
            } else {
                number1 = Double.parseDouble(editNumber1.getText().toString());
                number2 = Double.parseDouble(editNumber2.getText().toString());

                switch (v.getId()) {
                    case R.id.plus:
                        add();
                        Data.addList.add(" " + number1 + " + " + number2 + " = " + result);
                        Data.allList.add(" " + number1 + " + " + number2 + " = " + result);
                        break;
                    case R.id.minus:
                        subtract();
                        Data.subtractList.add(" " + number1 + " - " + number2 + " = " + result);
                        Data.allList.add(" " + number1 + " - " + number2 + " = " + result);
                        break;
                    case R.id.multiply:
                        multiply();
                        Data.multiplyList.add(" " + number1 + " * " + number2 + " = " + result);
                        Data.allList.add(" " + number1 + " * " + number2 + " = " + result);
                        break;
                    case R.id.divide:
                        divide();
                        Data.divideList.add(" " + number1 + " / " + number2 + " = " + result);
                        Data.allList.add(" " + number1 + " / " + number2 + " = " + result);
                        break;
                }

            }
        }
    }

    public Double add() {
        result = number1 + number2;
        editResult.setText("" + result);
        return result;
    }

    public Double subtract() {
        result = number1 - number2;
        editResult.setText("" + result);
        return result;
    }

    public Double multiply() {
        result = number1 * number2;
        editResult.setText("" + result);
        return result;
    }

    public Double divide() {
        if (number2 == 0) {
            Dialog dialog = new Dialog(this);
            dialog.setTitle("Cannot be divided by 0");
            dialog.show();
        } else {
            result = number1 / number2;
            editResult.setText("" + result);
            return result;
        }
        return null;
    }

    public void summary() {
        Intent intent = new Intent(MainActivity.this, com.example.piyush.calculator.util.SummaryActivity.class);
        startActivity(intent);
    }
}
