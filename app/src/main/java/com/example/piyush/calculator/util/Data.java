package com.example.piyush.calculator.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by PIYUSH on 28-01-2015.
 */
public abstract class Data {
    public static HashMap<String, ArrayList<String>> summaryMap = new HashMap<>();
    public static ArrayList<String> allList = new ArrayList<>();
    public static ArrayList<String> addList = new ArrayList<>();
    public static ArrayList<String> subtractList = new ArrayList<>();
    public static ArrayList<String> multiplyList = new ArrayList<>();
    public static ArrayList<String> divideList = new ArrayList<>();
}
